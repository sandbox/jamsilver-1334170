

(function ($) {

Drupal.wysiwyg.plugins['quote'] = {
  /**
   * Execute the button.
   */
  invoke: function(data, settings, instanceId) {
    var content = '[quote]'+data['content']+'[/quote]';
    if (typeof content != 'undefined') {
      Drupal.wysiwyg.instances[instanceId].insert(content);
    }
  }
};

})(jQuery);
